import requests
import settings
from xml.etree import ElementTree
from datetime import datetime
import re

RE_DATETIME = re.compile('\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}')

mapping = (('LinienText', 'Linie'), ('RichtungsText', 'Endstation'),
           ('AnkunftszeitAZBPlan', 'AnkunftPlan'), ('AnkunftszeitAZBPrognose', 'AnkunftPrognose'),
           ('AbfahrtszeitAZBPlan', 'AbfahrtPlan'), ('AbfahrtszeitAZBPrognose', 'AbfahrtPrognose'))

class Departure(dict):
    pass

class DepartureList(list):
    def __init__(self, sender, stop_id):
        self.timestamp = datetime.now()
        self._sender = sender
        if type(stop_id) in (list, tuple):
            self._stop_id = stop_id
        else:
            self._stop_id = [stop_id]
        self.refresh()
        
    def refresh(self):
        while len(self) > 0:
            self.pop()

        data = []
        for stop_id in self._stop_id:
            data += _load_dep_xml(self._sender, stop_id)

        if len(data) > 0 and data[0].attrib.has_key('Zst'):
            xml_timestamp = data[0].attrib['Zst']
            if RE_DATETIME.match(xml_timestamp):
                self.timestamp = datetime.strptime(xml_timestamp, '%Y-%m-%dT%H:%M:%S')
        else:
            print 'no timestamp found in xml'
                
        for departure in data:
            self.append(_xml_to_departure(departure))

        self.sort(key=lambda x: x['AbfahrtPrognose'])
        
def _xml_to_departure(entity):
    dep = Departure()
    for xelem, delem in mapping:
        e = entity.find('.//'+xelem)
        if e is not None:
            txt = e.text
            if RE_DATETIME.match(txt):
                txt = datetime.strptime(txt, '%Y-%m-%dT%H:%M:%S')
            dep[delem] = txt
    dep['lowfloor'] = entity.find('.//ServiceMerkmal') is not None
    
    return dep
    
def _load_dep_xml(sender, stop_id):
    d = {'sender':sender, 'haltepunkt':stop_id}
    x = requests.get(settings.baseurl, params=d)

    ux = x.text.encode('utf-8')
    et = ElementTree.fromstring(ux)

    return et.findall('.//AZBFahrplanlage')
