// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Component {
    Rectangle {
        id: depItem

        height: 100
        //color: "green"

        anchors {
            left: parent.left
            right: parent.right
        }

        Row {
            anchors {
                left: parent.left
                right: parent.right
            }

            spacing: 10

            Text {
                id: line
                width: 50
                horizontalAlignment: Text.AlignCenter
                anchors.verticalCenter: parent.verticalCenter
                text: model.line
            }

            Column {
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    id: station
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: model.station
                }
                Text {
                    id: target
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: model.direction
                }
            }

            Text {
                id: dep
                width: 50
                anchors.verticalCenter: parent.verticalCenter
                text: model.time
                horizontalAlignment: Text.AlignRight
                color: model.lowfloor ? 'blue' : 'black'
            }
        }
    }
}
