// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

ListModel {

    ListElement {
        line: "1"
        direction: "Prater Hauptallee"
        station: "Resslpark"
        time: 1
        lowfloor: true
    }
    ListElement {
        line: "U3"
        direction: "Simmering"
        station: "Erdberg"
        time: 5
        lowfloor: false
    }
    ListElement {
        line: "2"
        direction: "Rathaus"
        station: "Rathaus"
        time: 8
        lowfloor: true
    }
    ListElement {
        line: "U1"
        direction: "Reumannplatz"
        station: "Nestroyplatz"
        time: 15
        lowfloor: false
    }
}
