import QtQuick 1.1
import com.nokia.meego 1.0

import "UIConstants.js" as UIConstants
import "ExtrasConstants.js" as ExtrasConstants

Page {
    tools: noTools

    Rectangle {
        id: header
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: -1
        }
        border {
            color: 'black'
            width: 1
        }

        gradient: Gradient {
            GradientStop { position: 0; color: '#777' }
            GradientStop { position: 1; color: '#aaa' }
        }

        height: 80
        color: 'white'
	}
    
    Component {
        id: stationDelegate
        Item {
            width: parent.width
            height: 80
            
            BorderImage {
                anchors.fill: parent
                visible: mouseArea.pressed
                source: theme.inverted ? 'image://theme/meegotouch-list-inverted-background-pressed-vertical-center': 'image://theme/meegotouch-list-background-pressed-vertical-center'
            }
            
            Text {
                id: station
                text: model.station                
                anchors.centerIn: parent
                font.pixelSize: UIConstants.FONT_XLARGE
                font.family: ExtrasConstants.FONT_FAMILY_LIGHT
                color: !theme.inverted ? UIConstants.COLOR_FOREGROUND : UIConstants.COLOR_INVERTED_FOREGROUND
            }
            
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onClicked: {
                    resultModel.set_station(station.text)
                    pageStack.push(resultPage)
                }
            }
        }
    }
    
    ListView {
        anchors {
            top: header.bottom
            left: parent.left
            bottom: parent.bottom
            right: parent.right
        }
        snapMode: ListView.SnapToItem
        model: stationModel
        delegate: stationDelegate
    }
}

