from PySide.QtCore import Qt, QAbstractListModel, Slot, Signal, Property

class QmlDataModel(QAbstractListModel):
    '''Base class for 2d qml datamodels'''
    def __init__(self, keys, parent=None):
        super(QmlDataModel, self).__init__(parent)
        self._data = []
        self.keys = {}
        i = 1
        for key in keys:
            self.keys[Qt.UserRole + i] = key
            i += 1

        self.setRoleNames(self.keys)

    def rowCount(self, index):
        return len(self._data)

    def indexIsValid(self, index, role):
        if not index.isValid():
            return False

        if index.row() > len(self._data):
            return False

        if self.keys.has_key(role):
            return True
        else:
            return False

    def data(self, index, role):
        if not self.indexIsValid(index, role):
            return None
        else:
            return self._data[index.row()][role - (Qt.UserRole + 1)]

    def setData(self, data):
        self.beginResetModel()
        self._data = data
        self.endResetModel()
        

class QmlSelectionDialogDataModel(QmlDataModel):
    '''Base class for 2d qml datamodels for Seclection Dialogs
    contains workaround for fsckd implementation'''

    '''qml SelectionDialog requires a property count in order to display stuff
    also the callback for a valid selection has no model data available besides
    the selected index, so the model has to provide data methods'''
    def keyIndex(self, key):
        return [k for k, v in self.keys.iteritems() if v == key][0]

    def _count(self):
        return len(self._data)

    @Signal
    def count_changed(self): pass

    count = Property(int, _count, notify=count_changed)

    @Slot(int, unicode, result=unicode)
    def getString(self, index, roleString):
        print u'{0}: {1}'.format(roleString,self._data[index][self.keyIndex(roleString) - (Qt.UserRole + 1)])
        return self._data[index][self.keyIndex(roleString) - (Qt.UserRole + 1)]
