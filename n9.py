#!/usr/bin/env python

from moveoVindobona import realtime
import moveoVindobona.requests
import xml.etree.ElementTree
from qmlhacks import QmlDataModel, QmlSelectionDialogDataModel

import settings

import sys, os, csv, threading
from PySide.QtCore import QObject, Slot, Signal
from PySide.QtGui import QApplication
from PySide.QtDeclarative import QDeclarativeView

class DepartureModel(QmlDataModel):
    def __init__(self, station_dict, parent=None):
        keys = ('line','station','direction','time', 'lowfloor')
        super(DepartureModel, self).__init__(keys, parent)
        self._station_dict = station_dict

    @Slot()
    def clear(self):
        self.setData(())

    @Slot()
    def refresh(self):
        def refresh_async():
            try:
                self._dep_list.refresh()
                self.refresh_data()
            except moveoVindobona.requests.exceptions.RequestException as error:
                messageProxy.error.emit(str(error))
            except xml.etree.ElementTree.ParseError as error:
                messageProxy.error.emit(str(error))
            self.refresh_done.emit()
            
        threading.Thread(target=refresh_async).start()
            
    def refresh_data(self):
        self.setData(map(lambda x: (x["Linie"], self._station, x["Endstation"], 
            int((x["AbfahrtPrognose"] - self._dep_list.timestamp).seconds / 60),
            x["lowfloor"]), self._dep_list))
    
    @Slot(unicode)     
    def set_station(self, station):
        def set_station_async(station):
            self._station = station
            try:
                self._dep_list = realtime.DepartureList(settings.sender, self._station_dict[station])
                self.refresh_data()
            except moveoVindobona.requests.exceptions.RequestException as error:
                messageProxy.error.emit(str(error))
            except xml.etree.ElementTree.ParseError as error:
                messageProxy.error.emit(str(error))
            self.refresh_done.emit()        
        threading.Thread(target=set_station_async, args=(station,)).start()
    refresh_done = Signal()

class NearbyModel(QmlSelectionDialogDataModel):
    def __init__(self, stations_gps, parent=None):
        super(NearbyModel, self).__init__(('name',), parent)
        self._stations_gps = stations_gps
            
    @Slot(float, float)
    def update_nearby(self, lat, lon):
        self.setData(map(lambda x: (x['station'],),
            filter(lambda y: abs(y['lat'] - lat) < 0.004 and abs(y['lon'] - lon) < 0.004,
            self._stations_gps)))
        
class MessageProxy(QObject):
    error = Signal(str)
    
    def __init__(self, parent=None):
        super(MessageProxy, self).__init__(parent)

messageProxy = MessageProxy()

if __name__ == '__main__':
    stations = {}
    stations_gps = []
    csv_dict_reader = csv.DictReader(open(os.path.join(os.path.dirname(__file__), 'stations.csv'), 'rb'), delimiter=';')
    for csv_line in csv_dict_reader:
        langbez = csv_line['langbez'].decode("utf-8")
        if langbez not in stations:
            stations_gps.append({'station': langbez, 'lat': float(csv_line['X-Koordinaten'].replace(',','.')), 'lon': float(csv_line['Y-Koordinaten'].replace(',','.'))})
            stations[langbez] = (csv_line['haltepunkt'],)
        else:
            stations[langbez] = stations[langbez] + (csv_line['haltepunkt'],)
    
    app = QApplication(sys.argv)

    view = QDeclarativeView()
    
    # expose the object to QML
    context = view.rootContext()

    station_model = QmlDataModel(('station',))
    station_model.setData(map(lambda x: (x,),sorted(stations.keys())))
    nearbyModel = NearbyModel(stations_gps)
    resultModel = DepartureModel(stations)

    context.setContextProperty('stationModel', station_model)
    context.setContextProperty('resultModel', resultModel)
    context.setContextProperty('nearbyModel', nearbyModel)
    context.setContextProperty('messageProxy', messageProxy)

    # Assume test from source directory, use relative path
    view.setSource(os.path.join(os.path.dirname(__file__), 'qml/main.qml'))

    view.showFullScreen()
    sys.exit(app.exec_())

